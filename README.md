<<<<<<< HEAD
# [Site Pessoal](https://github.com/crisgit/site/)

  

  

[Site Pessoal](https://crisgit.github.io/site) é um tema de portfólio pessoal de uma Dev Girl Freelancer e que é criado em uma página para o [Bootstrap](http://getbootstrap.com/) criado pelo [Start Bootstrap](http://startbootstrap.com/). Este tema apresenta várias seções de conteúdo, uma grade de portfólio responsivo com efeitos de foco, modos de item de portfólio de página inteira, uma linha de tempo responsiva.  

## Preview

  

  

[![Agency Preview](https://crisgit.github.io/site/img/)](https://crisgit.github.io/site/)

  

  

**[View Live Preview](https://crisgit.github.io/site)**

  

  

## Status

  

  

[![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg)](https://raw.githubusercontent.com/BlackrockDigital/startbootstrap-agency/master/LICENSE)

  

[![npm version](https://img.shields.io/npm/v/startbootstrap-agency.svg)](https://www.npmjs.com/package/startbootstrap-agency)

  

[![Build Status](https://travis-ci.org/BlackrockDigital/startbootstrap-agency.svg?branch=master)](https://travis-ci.org/BlackrockDigital/startbootstrap-agency)

  

[![dependencies Status](https://david-dm.org/BlackrockDigital/startbootstrap-agency/status.svg)](https://david-dm.org/BlackrockDigital/startbootstrap-agency)

  

[![devDependencies Status](https://david-dm.org/BlackrockDigital/startbootstrap-agency/dev-status.svg)](https://david-dm.org/BlackrockDigital/startbootstrap-agency?type=dev)

  

  

## Download e Instalação

  

 
Para começar a usar esse modelo, escolha uma das seguintes opções para começar:
  

*  [Download the latest release on Start Bootstrap](https://startbootstrap.com/template-overviews/agency/)

  

* Install via npm: `npm i startbootstrap-agency`

  

* Clone the repo: `git clone https://github.com/BlackrockDigital/startbootstrap-agency.git`

  

*  [Fork, Clone, or Download on GitHub](https://github.com/BlackrockDigital/startbootstrap-agency)

  

  

## Uso

  

  

### Uso Básico

  

  

  
Após o download, basta editar os arquivos HTML e CSS incluídos no modelo em seu editor de texto favorito para fazer alterações. Estes são os únicos arquivos que você precisa se preocupar, você pode ignorar todo o resto! Para pré-visualizar as alterações que você faz no código, você pode abrir o arquivo `index.html` no seu navegador.

  

  

###  Uso Avançado

  

  

  
Após a instalação, execute `npm install` e execute o comando` gulp dev`, que abrirá uma prévia do modelo no navegador padrão, observará as alterações nos arquivos principais do modelo e atualizará o navegador quando as alterações forem salvas. Você pode ver o `gulpfile.js` para ver quais tarefas estão incluídas no ambiente de desenvolvimento.

  

  

####   Tarefas Gulp 

  

  

-    `gulp` é a tarefa padrão que constrói tudo

  

-  `gulp dev`   O browserSync abre o projeto no seu navegador padrão e ao vivo recarrega quando as alterações são feitas

  

-  `gulp sass`   compila arquivos SCSS em CSS

  

-  `gulp minify-css`   Minifica o arquivo CSS compilado

  

-  `gulp minify-js`   Minifica o arquivo JS de temas

  

-  `gulp copy` copia dependências de node_modules para o diretório de fornecedores

  

  

##   Erros e Problemas

  

  

Tem um bug ou um problema com este modelo? [Abra um novo problema](https://github.com/BlackrockDigital/startbootstrap-agency/issues)  aqui no GitHub ou deixe um comentário no [página de visão geral do modelo em Start Bootstrap](http://startbootstrap.com/template-overviews/agency/).

  

  

## Construções Personalizadas

  

  

Você pode contratar Start Bootstrap para criar uma compilação personalizada de qualquer modelo ou criar algo do zero usando o Bootstrap. Para mais informações, visite o site **[página de serviços de design personalizado](https://startbootstrap.com/bootstrap-design-services/)**.

  

  

##  Sobre

  

  

  
Start Bootstrap é uma biblioteca de código aberto de templates e temas gratuitos do Bootstrap. Todos os templates e temas gratuitos no Start Bootstrap são liberados sob a licença do MIT, o que significa que você pode usá-los para qualquer propósito, mesmo para projetos comerciais.

  

  

* https://startbootstrap.com

  

* https://twitter.com/SBootstrap

  

  

  
Start Bootstrap foi criado por e é mantido por**[David Miller](http://davidmiller.io/)**, Dono de [Blackrock Digital](http://blackrockdigital.io/).

  

  

* http://davidmiller.io

  

* https://twitter.com/davidmillerskt

  

* https://github.com/davidtmiller

  

  
Start Bootstrap é baseado no [Bootstrap](http://getbootstrap.com/) framework criado por [Mark Otto](https://twitter.com/mdo) e [Jacob Thorton](https://twitter.com/fat).

  

  

## Direitos autorais e licença

  

  

:copyright: Copyright 2013-2018 Blackrock Digital LLC. Code released under the [MIT](https://github.com/BlackrockDigital/startbootstrap-agency/blob/gh-pages/LICENSE) license.
=======
# meu-site



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/bartes_dev6/meu-site.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/bartes_dev6/meu-site/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
>>>>>>> a3449d06eaf730576509ef8d8cb23d92328931c1
